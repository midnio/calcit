import ast
import operator as oper
from pprint import pprint

def calcop(binop):
    ops = {
        ast.Add: oper.add,
        ast.Mult: oper.mul,
        ast.Sub: oper.sub,
        ast.Div: oper.truediv,
        ast.Mod: oper.mod
    }
    n1, n2 = None, None
    op = ops[type(binop.op)]
    if type(binop.left) == ast.Num:
        n1 = binop.left.n
    elif type(binop.left) == ast.BinOp:
        n1 = calcop(binop.left)
    if type(binop.right) == ast.Num:
        n2 = binop.right.n
    elif type(binop.right) == ast.BinOp:
        n2 = calcop(binop.right)
    return op(n1, n2)

def calcit(prob):
    AST = ast.parse(prob.strip()).body[0].value
    return calcop(AST)